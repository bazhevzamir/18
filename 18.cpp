﻿#include <iostream>
#include <string>
#include <limits>
#include <ctime>
#include <cstdlib>
#include <algorithm> 

class Player
{
private:
    std::string name; 
    int score; 

public:
    
    Player(const std::string& playerName, int playerScore) : name(playerName), score(playerScore) { }

   
    const std::string& GetName() const { return name; }
    int GetScore() const { return score; }
};


bool ComparePlayers(const Player* a, const Player* b)
{
    return a->GetScore() > b->GetScore(); 
}

int GetIntegerInput(const std::string& prompt, int maxValue)
{
    int input;
    while (true)
    {
        std::cout << prompt;
        if ((std::cin >> input) && (input >= 1) && (input <= maxValue))
        {
            return input; 
        }
        else
        {
            
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << "Invalid input. Please enter an integer between 1 and " << maxValue << ".\n";
        }
    }
}

int main()
{
    srand(static_cast<unsigned int>(time(0))); 

    std::string names[] = { "Flash", "SuperMan", "Spider-Man", "Batman", "Hulk", "Shazam", "Dead-Pool" };
    const int maxPlayers = sizeof(names) / sizeof(names[0]); 

    int playerCount = GetIntegerInput("Enter the number of players (1-7): ", maxPlayers);

    Player** players = new Player * [playerCount]; 

    
    for (int i = 0; i < playerCount; ++i)
    {
        std::string playerName = names[rand() % maxPlayers];
        int playerScore = rand() % 101; 
        players[i] = new Player(playerName, playerScore);
    }

   
    std::sort(players, players + playerCount, ComparePlayers);

   
    std::cout << "\nSorted Players (by score):\n";
    for (int i = 0; i < playerCount; ++i)
    {
        std::cout << "Player Name: " << players[i]->GetName() << ", Score: " << players[i]->GetScore() << std::endl;
    }

    
    for (int i = 0; i < playerCount; ++i)
    {
        delete players[i];
    }
    delete[] players;

    return 0;
}


